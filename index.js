const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors'); 
const productRoutes = require('./routes/productRoutes');
const userRoutes = require('./routes/userRoutes');

// Database connection
mongoose.connect('mongodb+srv://Myca:Password@cluster0.kmqaf.mongodb.net/capstone2?retryWrites=true&w=majority', {
	useNewUrlParser: true, 
	useUnifiedTopology: true,
	useFindAndModify: false   
	})

// server setup
  const app = express();
  app.use(cors()) 
  app.use(express.json());
  app.use(express.urlencoded({extended:true}));


 app.use("/products", productRoutes);
 app.use("/users", userRoutes);
  app.listen(process.env.PORT || 4000, () => {
 	console.log(`API is now online on port ${process.env.PORT || 4000}`)
 })

	mongoose.connection.once('open', () => console.log('Now connected to the database'));
