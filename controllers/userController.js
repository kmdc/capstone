const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/product')

module.exports.register = (params) => {
	let user = new User ({
		email: params.email,
		password: bcrypt.hashSync(params.password,10)
	})

	return user.save().then((user,err)=> {return (err) ? false : true})
}


module.exports.get = (body) => {
	return User.find({ email: body.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then( user => {
		if (user === null){
			return false
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if(isPasswordMatched){
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}

		console.log(user.toObject());

	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}


module.exports.checkout = async (data) =>{
	
	//Get the courseId and we save it in the enrollments array of the user
	let userSaveStatus = await User.findById(data.userId).then(user =>{
		user.order.push({productId: data.productId, quantity: data.quantity, totalAmount: data.price * data.quantity})
		

		

		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true
			}
		})
	
	})//the userSaveStatus tell us if the course was successfully added to the user

	let productSaveStatus = await Product.findById(data.productId).then( product =>{
		product.userOrder.push({userId: data.userId, quantity: data.quantity})
	
		return product.save().then((product, error)=>{
			if(error){
				return false;
			}else{
				return true
			}
		})
	
	})//the courseSaveStatus tells us if the user was successfully saved to the course


	//We have to check if the courseSaveStatus and userSaveStatus are both successful
	if(userSaveStatus && productSaveStatus){
		return true;
	}else {
		return false
	
 }
}


module.exports.orders = (params) => {
	
	 return User.find({}).then((data, err) => { 
	 	console.log(data)
	 	let allCustomers = [];
	 	
	 	let i = 0;

	 	while (i < data.length) {
	 		let allOrders = data[i].order.length;
	 		
	 		if (allOrders !== 0) {

	 		allCustomers.push(data[i].order)
	 	}
	 		i++;

	 	}  
        if (err) {
        	return false
        }else {
        	return allCustomers;
        }
})
}
