const Product = require('../models/product');
const auth = require('../auth');
const bcrypt = require('bcrypt');
const User = require('../models/user')

//create product no admin access
module.exports.addProducts = (body) => {

let newProduct = new Product({

name: body.name,
description: body.description,
price: body.price

});

return newProduct.save().then((productSaved, err) => {

return (err) ? false : true;
});

}
//create product with admin access
module.exports.addProductAdmin = (userId, body) => {
return User.findById(userId).then((result, err) =>{
if (err){

return false

}else{
if (result.isAdmin){
let newProduct = new Product({

name: body.name,
description: body.description,
price: body.price

});

return newProduct.save().then((productSaved, err) => {

return (err) ? false : productSaved;

});

}else{


return 'Not Admin'

}
}
})

}

//retrieve active products
module.exports.get = (params)=>{
return Products.find(params).then(products=>{

return products
})
}
//retrieve single products
module.exports.getProducts = () => {

return Products.find({}).then( (result, err) => {
if(err){
return console.log(err);
} else {
return result
}
});
};

module.exports.updateProducts = (productId, body)=>{
newCourse = {
name: body.name,
description: body.description,
price: body.price
}

return Products.findByIdAndUpdate(productId, newProduct,(err, success) =>{
if(err){
console.log(err);
return false;
}
else{

return true;
}

})
}
module.exports.archiveProduct = (productId,)=>{

return Products.findById(productId).then((result, error)=>{
if(error){
console.log(err);
return false;
}
//results of the findById will be placed in result

result.isActive = false;

return result.save().then((archiveSuccessful, saveErr)=>{
if(saveErr){
console.log(saveErr);
return false;

}else{
return true;
}

})
})