const Product = require('../models/product');


module.exports.addProduct = (params) =>{
	let newProduct = new Product({
		name: params.name,
		price: params.price
		
	})

	return newProduct.save().then((product, err)=>{
		return (err) ? false : true
	})
}

// Update product information 
module.exports.updateProduct = (productId, body) => {
	let newProduct = {  
		name: body.name,
		/*description : body.description,*/
		price: body.price
	};

	return Product.findByIdAndUpdate(productId, newProduct).then((result, err)=> {
		return result.save().then((result, err) => {
			return (err) ? false : true;
		});
	});
}

module.exports.getAllActive = (body) => {
	return Product.find( {isActive: true}).then(result => {
		return result
	})
}

module.exports.get = (body) => {
	return Product.find({body}).then(result => {
		return result
	})
}

module.exports.getById = (productId) => {

	return Product.findById(productId).then((product, error) => {
		if(error) {
			console.log(error);
			return false; 
			}else {
				console.log(product);
				return product;
			}
		
		})
}

module.exports.archive = (params) => {
    let active = { 
        isActive: false
    }

    return Product.findByIdAndUpdate(params.productId, active).then((result, err) => {
       
            return (err) ? false : true;
       

    })
}

module.exports.orders = (params) => {
	
	 return Product.find({}).then((data, err) => { 
	 	console.log(data)
	 	let allCustomers = [];

	 	let i = 0;

	 	while (i < data.length) {
	 		if (data[i].order.length !== 0) {

	 		allCustomers.push(data[i].order)
	 	}
	 		i++;

	 	} 
        
})
}
